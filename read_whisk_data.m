function [cities, M_prime, Gamma, Ac, At, eps, Phi, T] = read_whisk_data(filename)
%read_whisk_data.m
%   Reads the data from the data table written in .csv file.
TT = readtable(filename);
cities = TT.City;

% T is 1 by default
T = 1;

% B is a boolean matrix. The constant values can be changed if necessary.

M_prime_high =  2.5;
M_prime_low =   0.9;
Gamma_high =        3;
Gamma_low =         1;
Ac_high =       0.3;
Ac_low =        0.05;
At_high =       -0.3;
At_low =        -0.05;
eps_high =      1;
eps_low =       0.2;
Phi_high =      1.3;
Phi_low =       1;

B_M_prime_2 = TT.M_prime == -1;         % Collection of -1
B_M_prime_1 = TT.M_prime + B_M_prime_2; % Collection of 1
M_prime = M_prime_high*B_M_prime_1 + M_prime_low*B_M_prime_2;

B_Gamma_2 = TT.Gamma == -1;
B_Gamma_1 = TT.Gamma + B_Gamma_2;
Gamma = Gamma_high*B_Gamma_1 + Gamma_low*B_Gamma_2;

B_Ac_2 = TT.Ac == -1;
B_Ac_1 = TT.Ac + B_Ac_2;
Ac = Ac_high*B_Ac_1 + Ac_low*B_Ac_2;

B_At_2 = TT.At == -1;
B_At_1 = TT.At + B_At_2;
At = At_high*B_At_1 + At_low*B_At_2;

B_eps_2 = TT.eps == -1;
B_eps_1 = TT.eps + B_eps_2;
eps = eps_high*B_eps_1 + eps_low*B_eps_2;

B_Phi_2 = TT.Phi == -1;
B_Phi_1 = TT.Phi + B_Phi_2;
Phi = Phi_high*B_Phi_1 + Phi_low*B_Phi_2;

end

