% Points Generation Function
% alpha = rotated angle of elliptic 1 in deg.;
% beta = rotated angle of elliptic 2 in deg.;
% alpha,beta \in (-90,90)
% M = distance of center point of two elliptics;
% a = long-axis of elliptic 1;
% b = short-axis of elliptic 1;
% k = long-axis of elliptic 2;
% l = short-axis of elliptic 2;
% example
% [x,y,z,t] = find_surface_sin(30,-10,20,10,5,7,3)
% [x,y,z] is the value of point samples 
% t is computation consumption

function [x,y,z,t] = find_surface_sin(alpha,beta,M,a,b,k,l)
tic;
% Projection on alpha=0/beta=0 surface
a_prime = a*cosd(alpha);
k_prime = k*cosd(beta);

% Adjust point density if you want higher surface quality
density = 500;
theta = linspace(0,180,181); % Think of top half of the geometry( symmetric with z-x plane)

% Space for saving point samples
x = zeros(length(theta),density);
% x_1 = zeros(length(theta),density/2);
% x_2 = zeros(length(theta),density/2);
y = zeros(length(theta),density);
% y_1 = zeros(length(theta),density/2);
% y_2 = zeros(length(theta),density/2);
z = zeros(length(theta),density);
z_1 = zeros(length(theta),density/2);
z_2 = zeros(length(theta),density/2);

% find value of point samples
for i=1:length(theta)
    if theta(i)<90
        X_1 = sqrt(1/(1/a_prime^2+(tand(theta(i))/b)^2));
        Y_1 = tand(theta(i))*X_1;
        X_2 = sqrt(1/(1/k_prime^2+(tand(theta(i))/l)^2));
        Y_2 = tand(theta(i))*X_2;
    elseif theta(i)==90
        X_1 = 0;
        Y_1 = b;
        X_2 = 0;
        Y_2 = l;
    elseif theta(i)>90
        X_1 = -sqrt(1/(1/a_prime^2+(tand(theta(i))/b)^2));
        Y_1 = tand(theta(i))*X_1;
        X_2 = -sqrt(1/(1/k_prime^2+(tand(theta(i))/l)^2));
        Y_2 = tand(theta(i))*X_2;
    end
        
    Z_1 = X_1*tand(alpha);
    Z_2 = M+X_2*tand(beta);
    Z_3 = Z_1+2*M;
    R_1 = sqrt(X_1^2+Y_1^2);
    R_2 = sqrt(X_2^2+Y_2^2);
    M_1 = Z_2-Z_1;
    M_2 = 2*M-M_1;
    lam_1 = 2*M_1;
    lam_2 = 2*M_2;
    T_1 = 2*pi/(lam_1);
    T_2 = 2*pi/(lam_2);
    z_1(i,:) = linspace(Z_1,Z_2,density/2);
    z_2(i,:) = linspace(Z_2,Z_3,density/2);
    z(i,:) = [z_1(i,:),z_2(i,:)];
    r_1 = (R_1-R_2)/2*cos(T_1*(z_1(i,:)-Z_1))+(R_1+R_2)/2;
    r_2 = (R_1-R_2)/2*-cos(T_2*(z_2(i,:)-Z_2))+(R_1+R_2)/2;
    r = [r_1,r_2];
%     x_1(i,:) = r_1*cosd(theta(i));
%     x_2(i,:) = r_2*cosd(theta(i));
%     y_1(i,:) = r_1*sind(theta(i));
%     y_2(i,:) = r_2*sind(theta(i));
    x(i,:) = r*cosd(theta(i));
    y(i,:) = r*sind(theta(i));
end

% Check if the geometry exists
XNaN = isnan(x);
YNaN = isnan(y);
ZNaN = isnan(z);
bx = sum(XNaN,'all');
by = sum(YNaN,'all');
bz = sum(ZNaN,'all');
if bx~=0 ||by~=0 || bz~=0
    msg = "The geometry does not exit for these input parameters.";
    error(msg);
end

x=[x,x,x,x];
y=[y,y,y,y];
z=[z-2*M,z,z+2*M,z+4*M];

% Plots the surface

 plot_surface(x,y,z,M);

% figure(1)
% s_1 = surf(z,x,y);
% s_1.EdgeColor = 'none';
% hold on
% s_2 = surf(z,x,-y);
% s_2.EdgeColor = 'none';
% axis equal
% xlim([0,4*M])
% xlabel('z-direction')
% ylabel('x-direction')
% zlabel('y-direction')
% hold off

% figure(2),
% s_3 = surf(z_1,x_1,y_1);
% s_3.EdgeColor = 'none';
% hold on
% s_4 = surf(z_1,x_1,-y_1);
% s_4.EdgeColor = 'none';
% axis equal
% xlabel('z-direction')
% ylabel('x-direction')
% zlabel('y-direction')
% hold off
% 
% figure(3),
% s_5 = surf(z_2,x_2,y_2);
% s_5.EdgeColor = 'none';
% hold on;
% s_6 = surf(z_2,x_2,-y_2);
% s_6.EdgeColor = 'none';
% axis equal
% xlabel('z-direction')
% ylabel('x-direction')
% zlabel('y-direction')
% hold off
 t=toc;
end