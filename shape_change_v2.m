% t_0 is the start of computational time
% t_trans is transitional time
% step is the numbers of increment
% Example 
% shape_change_v2('Chicago','Fresno',100,200,200)
function shape_change_v2(city_1,city_2,t_0,t_trans,step)
%shape_change_test.m Summary of this function goes here
%   Tests smooth transitions of varaiables
% Read the whisker data
filename = 'whisk_data.csv';
[cities, M_prime, Gamma, Ac, At, eps, Phi, ~] = read_whisk_data(filename);
T = table(cities, M_prime, Gamma, Ac, At, eps, Phi);
T.Properties.RowNames = cities;
Tnew = T({city_1,city_2},:);
[alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1] = ...
    conversion(table2array(Tnew(1,2)),table2array(Tnew(1,3)),...
    table2array(Tnew(1,4)),table2array(Tnew(1,5)),table2array(Tnew(1,6)), ...
    table2array(Tnew(1,7)));
[alpha_2,beta_2,M_2,a_2,b_2,k_2,l_2] = ...
    conversion(table2array(Tnew(2,2)),table2array(Tnew(2,3)),...
    table2array(Tnew(2,4)),table2array(Tnew(2,5)),table2array(Tnew(2,6)), ...
    table2array(Tnew(2,7)));

% Set the constants to define the plot region
time = linspace(1, t_trans, step);
time_append = linspace(0, t_0, t_0+1);
t = [time_append, time+t_0, time_append(2:end)+t_0+t_trans];
t_len = length(t);

[Location_1(:,:,1),Location_1(:,:,2),Location_1(:,:,3)]...
    = find_surface_sin_v(alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1);
[Location_2(:,:,1),Location_2(:,:,2),Location_2(:,:,3)]...
    = find_surface_sin_v(alpha_2,beta_2,M_2,a_2,b_2,k_2,l_2);
V = Location_2-Location_1;

% Define the smooth tanh(sinh()) curve to represent the continuous
% transition of the paramters.
expand = 1.0000001; % The expansion constant for the hyperbolic funtion

% Calculate the positions of the coordinate
% I = zeros(1,t_len);
% for i=1:t_len
%     if t(i)<=t_0
%         I(i) = 0;
%     elseif t(i)>t_0 && t(i)<t_0+t_trans
%         I(i) = expand/2*tanh(sinh((asinh(atanh(1/expand))-asinh(atanh(-1/expand)))/t_trans*(t(i)-t_0)+asinh(atanh(-1/expand))))+1/2;
%     elseif t(i)>=t_0+t_trans
%         I(i) = 1;
%     end
%     Location{i} = Location_1+V*I(i);
% %     M(i) = M_1+(M_2-M_1)*I(i);
% end

t_1 = length(time_append);
t_2 = t_1 + length(time);
I_2 = expand./2.*tanh(sinh((asinh(atanh(1./expand))-asinh(atanh(-1./expand)))./t_trans.*(t(t_1+1:t_2-1)-t_0)+asinh(atanh(-1./expand))))+1/2;
I = [zeros(1,t_1), I_2, ones(1,t_1)];
M = M_1+(M_2-M_1).*I;
Location = cell(1,t_len);
for i=1:t_len
    Location{i} = Location_1+V*I(i);
end

% Clear figures
close all;

% Plot the displacement vector
figure(1)
plot(t,I)
title('Coefficient of Displacement Vector vs. Time');
xlabel('Time')
ylabel('Coefficient of Displacement Vector')

% Plot and animate the transition
figure(2)
xlim([0,4*M(1)])
an = annotation('textbox',[.2 .5 .3 .3],'String',...
    ['t = ', num2str(0)],'EdgeColor','none');
s_1 = surf(Location{1}(:,:,3),Location{1}(:,:,1),...
    Location{1}(:,:,2));
s_1.EdgeColor = 'none';
hold on
s_2 = surf(Location{1}(:,:,3),Location{1}(:,:,1),...
    -Location{1}(:,:,2));
s_2.EdgeColor = 'none';
axis equal
title(['Transition from ', city_1,' to ',city_2]);
xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')
hold off
for i = 1:t_len
    figure(2)
    delete(an)
    xlim([0,4*M(i)])
    s_1.XData = Location{i}(:,:,3);
    s_1.YData = Location{i}(:,:,1);
    s_1.ZData = Location{i}(:,:,2);
    s_2.XData = Location{i}(:,:,3);
    s_2.YData = Location{i}(:,:,1);
    s_2.ZData = -Location{i}(:,:,2);
    an = annotation('textbox',[.2 .5 .3 .3],'String',...
        ['t = ',num2str(t(i))], 'EdgeColor','none'); 
end


end