function [] = comp_coords_sin()
%comp_coords.m Summary of this function goes here
%   Compares the coordinates generated by MATLAB and SOLIDWORKS

% Read the data
inpdat = 'surfacePoints_wichita';
A = read_swdata(inpdat);

X_prime = A(1,:);
Y_prime = A(2,:);
Z_prime = A(3,:);
% axis equal
% scatter3(Z_prime,X_prime,Y_prime);

% Get the constants ready for Wichita
[cities, M_prime, Gamma, Ac, At, eps, Phi, T] ...
    = read_whisk_data('whisk_data.csv');
[alpha,beta,M,a,b,k,l] = conversion(M_prime,Gamma,Ac,At,eps,Phi,T);
alpha = alpha(5);
beta = beta(5);
M = M(5);
a = a(5);
b = b(5);
k = k(5);
l = l(5);
% [~,~,~,~] = find_surface(alpha,beta,M,a,b,k,l);

% The center part accuracy is more important. Analyze only the M<z<2M part
% of the coorrdinates.
A = A(:, M<=Z_prime & Z_prime<3*M);
X = A(1,:);
Y = A(2,:);
Z = A(3,:);

% find the negative x and y coordinates and thetas over 180
Y = abs(Y);
theta_1 = atand(Y./X);
theta_neg_loc = theta_1 < 0;
theta = theta_1 + 180*theta_neg_loc;

% Compute the x and y based on z and theta
P = [X;Y;Z;theta];
L_1 = a*sind(alpha);
L_2 = k*sind(beta);
P1 = P(:,theta<90);
P2 = P(:,theta>90);
P3 = P(:,theta==90);
% Sort the coordinates into calculation regions
zeta1 = M + L_1;
zeta2 = 2*M - L_2;
zeta3 = 2*M + L_2;
zeta4 = 3*M - L_1;
Z1 = P1(3,:);
Z2 = P2(3,:);
Z3 = P3(3,:);
P11 = P1(:,Z1<zeta1);
P12 = P1(:,Z1>=zeta1 & Z1<zeta3);
P13 = P1(:,Z1>=zeta3);
P14 = P12(:,P12(3,:)>=2*M);
P21 = P2(:,Z2<zeta2);
P22 = P2(:,Z2>=zeta2 & Z2<zeta4);
P23 = P2(:,Z2>=zeta4);
P24 = P22(:,P22(3,:)<2*M);
P3_cal1 = P3(:, Z3<2*M);
P3_cal2 = P3(:,Z3>=2*M);
% Define the calculation regions
P1_cal1 = P11(:,(P11(3,:)-M)<P11(1,:)*tand(alpha));
P1_cal2 = [P11(:,(P11(3,:)-M)>=P11(1,:)*tand(alpha)), ...
    P12(:,P12(3,:)<2*M), P14(:,(P14(3,:)-2*M)<P14(1,:)*tand(beta))];
P1_cal3 = [P14(:,(P14(3,:)-2*M)>=P14(1,:)*tand(beta)),P13];
P2_cal1 = [P21, P24(:,P24(3,:)<(P24(1,:)*tand(beta)+M))];
P2_cal2 = [P24(:,P24(3,:)>=(P24(1,:)*tand(beta)+M)),P22(:,P22(3,:)>=2*M)...
    P23(:,P23(3,:)<(P23(1,:)*tand(beta)+2*M))];
P2_cal3 = P23(:,P23(3,:)>=(P23(1,:)*tand(beta)+2*M));
% Calculate and compare.
tol = 0.03;
[P1_inside1,P1_outside1,dr11,max_dr_11,mean_dr11] = ...
    check_r_sin(P1_cal1,alpha,beta,M,a,b,k,l,tol,-M,0,1);
[P1_inside2,P1_outside2,dr12,max_dr_12,mean_dr12] = ...
    check_r_sin(P1_cal2,alpha,beta,M,a,b,k,l,tol,M,0,0);

end

