function [A] = read_swdata(inpdat)
%read_data.m Summary of this function goes here
%   Reads SOLIDWORKS-generated data
fid = fopen(['coords/',inpdat],'r');
formatspec = '%*[^\n]%f %f %f %*f\n';
sizeA = [3 Inf];
frewind(fid);
fgetl(fid);
fgetl(fid);

A = fscanf(fid,formatspec,sizeA);
fclose(fid);
end

