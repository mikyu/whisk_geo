function [P_in,P_out,dr,max_dr,mean_dr] = ...
    check_r_spl(P,alpha,beta,M,a,b,k,l,tol,dz,deg)
%UNTITLED2 Summary of this function goes here
%    Check if the given coordinates are within the tolerance

if deg~=0 && deg~=1 && deg~=2
    msg1 = ["deg should be either 0, 1, or 2; 0 for 0 <= theta < 90,", ...
        "1 for theta == 90, and 2 for 90 < theta <= 180"];
    error(msg1);
end

% Mostly the duplicate of find_surface.m
a_prime = a*cosd(alpha);
k_prime = k*cosd(beta);

theta = P(4,:);
if deg == 0
    X_1 = sqrt(1./(1./a_prime^2 + (tand(theta)./b).^2));
    X_2 = sqrt(1./(1./k_prime^2 + (tand(theta)./l).^2));
    Y_1 = tand(theta).*X_1;
    Y_2 = tand(theta).*X_2;
elseif deg == 1
    X_1 = 0;
    X_2 = 0;
    Y_1 = b;
    Y_2 = l;
elseif deg ==2
    X_1 = -sqrt(1./(1./a_prime^2 + (tand(theta)./b).^2));
    X_2 = -sqrt(1./(1./k_prime^2 + (tand(theta)./l).^2));
    Y_1 = tand(theta).*X_1;
    Y_2 = tand(theta).*X_2;
end 

Z_1 = X_1.*tand(alpha);
Z_2 = M + X_2.*tand(beta);
Z_3 = Z_1 + 2*M;
R_1 = sqrt(X_1.^2 + Y_1.^2);
R_2 = sqrt(X_2.^2 + Y_2.^2);

R = zeros(1,length(theta));
for i=1:length(theta)
    z_1 = Z_1(i);
    z_2 = Z_2(i);
    r_1 = R_1(i);
    r_2 = R_2(i);
    z_pts = [z_2-4*M, z_1-2*M, z_2-2*M, z_1, z_2, z_1+2*M, z_2+2*M, ...
        z_1+4*M, z_2+4*M, z_1+6*M, z_2+6*M];
    r_pts = [r_2, r_1, r_2, r_1, r_2, r_1, r_2, r_1, r_2, r_1, r_2];
    R(i) = spline(z_pts,r_pts, P(3,i)-dz);
end

% Comapre the given values are within the tolerance
r = sqrt(P(1,:).^2 + P(2,:).^2);
r_lar = r + tol;
r_sma = r - tol;

P = vertcat(P,R,r,r_lar,r_sma);
r_in_loc = R <= r_lar & R >= r_sma;
r_out_loc = R > r_lar | R < r_sma;
dr = abs(R - r);
max_dr = max(dr);
mean_dr = mean(dr);
P_in = P(:,r_in_loc);
P_out = P(:,r_out_loc);

end

