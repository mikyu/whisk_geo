function [] = comp_splines()
%comp_splines.m Summary of this function goes here
%   Compare the spline plots of the SW and ML models

% Read Wichita data
inpdat = 'surfacePoints_wichita';
A = read_swdata(inpdat);

X_prime = A(1,:);
Y_prime = A(2,:);
Z_prime = A(3,:);

% Get the constants ready for Wichita
[cities, M_prime, Gamma, Ac, At, eps, Phi, T] ...
    = read_whisk_data('whisk_data.csv');
[alpha,beta,M,a,b,k,l] = conversion(M_prime,Gamma,Ac,At,eps,Phi,T);
alpha = alpha(5);
beta = beta(5);
M = M(5);
a = a(5);
b = b(5);
k = k(5);
l = l(5);

% The center part accuracy is more important. Analyze only the M<z<2M part
% of the coorrdinates.
A = A(:, M<=Z_prime & Z_prime<3*M);
X = A(1,:);
Y = A(2,:);
Z = A(3,:);

% find the negative x and y coordinates and thetas over 180
Y = abs(Y);
theta_1 = atand(Y./X);
theta_neg_loc = theta_1 < 0;
theta = theta_1 + 180*theta_neg_loc;

% Compute the x and y based on z and theta
P = [X;Y;Z;theta];
L_1 = a*sind(alpha);
L_2 = k*sind(beta);
P1 = P(:,theta<90);
P2 = P(:,theta>90);
P3 = P(:,theta==90);

% Calculate and compare
tol = 0.003;
[P1_inside,P1_outside,dr1,max_dr_1,mean_dr1] = ...
    check_r_spl(P1,alpha,beta,M,a,b,k,l,tol,-M,0);
[P2_inside,P2_outside,dr2,max_dr_2,mean_dr2] = ...
    check_r_spl(P2,alpha,beta,M,a,b,k,l,tol,M,2);
[P3_inside,P3_outside,dr3,max_dr_3,mean_dr3] = ...
    check_r_spl(P3,alpha,beta,M,a,b,k,l,tol,-M,1);

end

