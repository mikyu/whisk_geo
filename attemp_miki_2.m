function [t] = attemp_miki_2(filename)
%attempt_miki_2.m Summary of this function goes here
%   A test function to generate the whisker plots and coords
tic;

%filename = 'whisk_data.csv';
[cities, M_prime, Gamma, Ac, At, eps, Phi, T] = read_whisk_data(filename);

global city;
for i=1:length(cities)
    city = cities{i};
    [~,~,~,~]=whisker_gen(M_prime(i),Gamma(i),Ac(i),At(i),eps(i),Phi(i),T);
end

t = toc;
end

