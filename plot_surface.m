function [] = plot_surface(x,y,z,M)
%plot_surface.m Summary of this function goes here
%   Plots the surface of a whisker

global city;
if ischar(city)
%     figure(city_and_number{2})
    figure('Name',city,'NumberTitle','off');
    title(city);
else
    % figure
end

s_1 = surf(z,x,y);
s_1.EdgeColor = 'none';
hold on
s_2 = surf(z,x,-y);
s_2.EdgeColor = 'none';
axis equal
xlim([-M,3*M])
xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')

hold off

% figure(2),
% s_3 = surf(z_1,x_1,y_1);
% s_3.EdgeColor = 'none';
% hold on
% s_4 = surf(z_1,x_1,-y_1);
% s_4.EdgeColor = 'none';
% axis equal
% xlabel('z-direction')
% ylabel('x-direction')
% zlabel('y-direction')
% hold off
% 
% figure(3),
% s_5 = surf(z_2,x_2,y_2);
% s_5.EdgeColor = 'none';
% hold on;
% s_6 = surf(z_2,x_2,-y_2);
% s_6.EdgeColor = 'none';
% axis equal
% xlabel('z-direction')
% ylabel('x-direction')
% zlabel('y-direction')
% hold off
end

