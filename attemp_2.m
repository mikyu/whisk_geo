% 'step' = number of steps;
% 'time' = the time gap between each step

function attemp_2(M_prime_0,Gamma_0,Ac_0,At_0,eps_0,Phi_0,T_0,M_prime_1,Gamma_1,Ac_1,At_1,eps_1,Phi_1,T_1,step,time,)

[a_0,b_0,k_0,l_0,M_0,alpha_0,beta_0] = conversion(M_prime_0,Gamma_0,Ac_0,At_0,eps_0,Phi_0,T_0);
[a_1,b_1,k_1,l_1,M_1,alpha_1,beta_1] = conversion(M_prime_1,Gamma_1,Ac_1,At_1,eps_1,Phi_1,T_1);
shape_change(alpha_0,beta_0,M_0,a_0,b_0,k_0,l_0,alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1,step,time)

end