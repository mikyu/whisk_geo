function [tt,Location] = shape_change_v1(city_1,city_2,t_0,t_trans,step)
%shape_change_test.m Summary of this function goes here
%   Tests smooth transitions of varaiables
%     t_0 is the start of computational time
%     t_trans is transitional time
%     step is the numbers of increment
%     Example 
%     shape_change_v1('Chicago','Fresno',100,200,200)

tic;

% Read the whisker data
filename = 'whisk_data.csv';
[cities, M_prime, Gamma, Ac, At, eps, Phi, ~] = read_whisk_data(filename);
T = table(cities, M_prime, Gamma, Ac, At, eps, Phi);
T.Properties.RowNames = cities;
Tnew = T({city_1,city_2},:);
[alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1] = ...
    conversion(table2array(Tnew(1,2)),table2array(Tnew(1,3)),...
    table2array(Tnew(1,4)),table2array(Tnew(1,5)),table2array(Tnew(1,6)), ...
    table2array(Tnew(1,7)));
[alpha_2,beta_2,M_2,a_2,b_2,k_2,l_2] = ...
    conversion(table2array(Tnew(2,2)),table2array(Tnew(2,3)),...
    table2array(Tnew(2,4)),table2array(Tnew(2,5)),table2array(Tnew(2,6)), ...
    table2array(Tnew(2,7)));

% Set the constants to define the plot region
const_width = 50;  % Width before and after the transitional area
time = linspace(0, t_trans, step);
time_append = linspace(0, const_width, round(step/t_trans*const_width));

% Define the smooth tanh(sinh()) curve to represent the continuous
% transition of the paramters.
expand = 1.00001; % The expansion constant for the hyperbolic funtion
t_1 = asinh(atanh(-1/expand));
t_2 = asinh(atanh(1/expand));
% Define the constant and transitional curve as a matrix.
tanh_sinh = @(inc_1,inc_2)[repmat(inc_1,1,length(time_append)), ...
    (inc_2-inc_1)*expand/2*tanh(sinh((t_2-t_1)/t_trans*time+t_1))+(inc_2+inc_1)/2, ...
    repmat(inc_2,1,length(time_append))];
t = t_0 - const_width + [time_append, time+const_width, time_append+const_width+t_trans];

alpha = tanh_sinh(alpha_1,alpha_2);
beta = tanh_sinh(beta_1,beta_2);
M    = tanh_sinh(M_1,M_2);
a    = tanh_sinh(a_1,a_2);
b    = tanh_sinh(b_1,b_2);
k    = tanh_sinh(k_1,k_2);
l    = tanh_sinh(l_1,l_2);

% Calculate the positions of the coordinate
Location = cell(length(t),3);
x_i = zeros(1,length(t));
y_i = zeros(1,length(t));
z_i = zeros(1,length(t));

time_append_len = length(time_append);
time_len = length(time);
t_len = length(t);

for i=1:t_len
    [Location{i,1},Location{i,2},Location{i,3}]...
        = find_surface_sin_v(alpha(i),beta(i),M(i),a(i),b(i),k(i),l(i));
    x_i(i) = Location{i,1}(1);
    y_i(i) = Location{i,2}(1);
    z_i(i) = Location{i,3}(1);
end

% Clear figures
clf

% Plot dX_0 vs. t
dX = sqrt(x_i.^2+y_i.^2+z_i.^2);
figure(1);
plot(t,dX);
title('dX_0 vs. t');
xlabel('t');
ylabel('dX_0');

% Plot the Trajectory of X_0(t)
figure(2);
plot3(z_i,x_i,y_i);
grid on
axis equal
title('Trajectory of X_0(t)');
xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')

% Plot and animate the transition
figure(3)
an = annotation('textbox',[.2 .5 .3 .3],'String',...
    ['t = ', num2str(time_append_len)],'EdgeColor','none');
s_1 = surf(Location{time_append_len,3},Location{time_append_len,1},...
    Location{time_append_len,2});
s_1.EdgeColor = 'none';
hold on
s_2 = surf(Location{time_append_len,3},Location{time_append_len,1},...
    -Location{time_append_len,2});
s_2.EdgeColor = 'none';
axis equal
title(['Transition from ', city_1,' to ',city_2]);
xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')
hold off
for i = time_append_len+1:time_len+time_append_len
    figure(3)
    xlim([-M(i),3*M(i)])
    delete(an)
    s_1.XData = Location{i,3};
    s_1.YData = Location{i,1};
    s_1.ZData = Location{i,2};
    s_2.XData = Location{i,3};
    s_2.YData = Location{i,1};
    s_2.ZData = -Location{i,2};
    an = annotation('textbox',[.2 .5 .3 .3],'String',...
        ['t = ',num2str(i)], 'EdgeColor','none'); 
end
tt = toc;
end

