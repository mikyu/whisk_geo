function [p_surf] = modSeal_test()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
a = 80;
b = 80;
k = 80;
l = 80;
alpha = 1.2;
beta = 1.2;
M = 5;
p = [-0.05877620948  0.4965333395    3.880597015;
    -0.4997597424   0.01549838456   4.179104478;
    0.3175804239    -0.386189946    11.34328358    ];
x_big = 0;
y_big = 0;
x_sml = 0;
y_sml = 0;
r = 0;

[sz,~] =size(p);
p_surf = zeros(size(p));

zs = zeros(1,sz);
for i = 1:sz
    pt_z = p(i,3);
    zs(i) = pt_z;
end

maxz = zs(1);
minz = zs(1);
for i = 1:sz
   if maxz < zs(i)
       maxz = zs(i);
   end
   if minz >zs(i)
       minz = zs(i);
   end
end

delta_z = maxz-minz;

for i = 1:sz
    zs(i) = 4*M/delta_z*(zs(i)-minz);
end
%{
if (delta_z ~= 4*M)
    error("length should match");
end
%}

a_prm = a*cos(alpha);
k_prm = k*cos(beta);

for i = 1:sz
   z = zs(i);
   pt = p(i,:);
   xp = pt(1);
   yp = pt(2);
   theta = sign(yp)*acos(xp/(sqrt(xp^2+yp^2)));
   if (0<=theta && theta<pi/2) || (pi<=theta && theta<3/2*pi)
       x_big = sqrt(1/(1/a_prm^2+((tan(theta))/b)^2));
       y_big = x_big*tan(theta);
       x_sml = sqrt(1/(1/k_prm^2+((tan(theta))/l)^2));
       y_sml = x_sml*tan(theta);
   elseif theta == pi/2 || theta == 3/2*pi
       x_big = 0;
       y_big = b;
       x_sml = 0;
       y_sml = l;
   elseif (pi/2<theta && theta<pi) || (3/2*pi<theta && theta<2*pi)
       x_big = -sqrt(1/(1/a_prm^2+((tan(theta))/b)^2));
       y_big = x_big*tan(theta);
       x_sml = -sqrt(1/(1/k_prm^2+((tan(theta))/l)^2));
       y_sml = x_sml*tan(theta);
   end
   
   z_a = x_big*tan(alpha);
   z_b = M + x_sml*tan(beta);
   z_c = z_a + 2*M;
   
   M_a = z_b -z_a;
   lam_a = 2*M_a;
   T_a = 2*pi/lam_a;
   M_b = 2*M-z_b;
   lam_b = 2*M_b;
   T_b = 2*pi/lam_b;
   R_big = sqrt(x_big^2+y_big^2);
   R_sml = sqrt(x_sml^2+y_sml^2);
   
   if z<z_a
       r = (R_big-R_sml)/2*cos(T_b*(z-(z_b-2*M))) + (R_big+R_sml)/2;
   elseif z_a<=z && z<z_b
       r = (R_big-R_sml)/2*cos(T_a*(z-z_a)) + (R_big+R_sml)/2;
   elseif z_b<=z && z<z_c
       r = (R_big-R_sml)/2*cos(T_b*(z-z_b)) + (R_big+R_sml)/2;
   elseif z_c<=z && z<(z_b+2*M)
       r = (R_big-R_sml)/2*cos(T_a*(z-z_c)) + (R_big+R_sml)/2;
   elseif (z_b+2*M)<=z && z<(z_c+2*M)
       r = (R_big-R_sml)/2*cos(T_b*(z-(z_b+2*M))) + (R_big+R_sml)/2;
   elseif (z_a+4*M)<=z && z<(z_b+4*M)
       r = (R_big-R_sml)/2*cos(T_a*(z-(z_a+4*M))) + (R_big+R_sml)/2;
   %{
elseif (z_b+4*M)<=z && z<(z_c+4*M)
       r = (R_big-R_sml)/2*cos(T_b*(z-(z_b+4*M))) + (R_big+R_sml)/2;
   elseif (z_a+6*M)<=z && z<(z_b+6*M)
       r = (R_big-R_sml)/2*cos(T_a*(z-(z_a+6*M))) + (R_big+R_sml)/2;
   elseif (z_b+6*M)<=z && z<(z_c+6*M)
       r = (R_big-R_sml)/2*cos(T_b*(z-(z_b+6*M))) + (R_big+R_sml)/2;
   %}
   end
   
   x = r*cos(theta);
   y = r*sin(theta);
   p_surf(i,1) = x;
   p_surf(i,2) = y;
   p_surf(i,3) = zs(i);
end
   
end

