function [x,y,z,t] = whisker_gen(M_prime,Gamma,Ac,At,eps,Phi,T)
%whisker_gen Summary of this function goes here
%   Generates the coordinates and plot of a whisker based on the
%   nondimensional paramters.

tic;

if nargin<7
    T = 1;
end

[alpha,beta,M,a,b,k,l] = conversion(M_prime,Gamma,Ac,At,eps,Phi,T);
[x,y,z,~] = find_surface(alpha,beta,M,a,b,k,l);

t = toc;

end

