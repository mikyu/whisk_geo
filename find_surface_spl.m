% Points Generation Function
% alpha = rotated angle of elliptic 1 in deg.;
% beta = rotated angle of elliptic 2 in deg.;
% alpha,beta \in (-90,90)
% M = distance of center point of two elliptics;
% a = long-axis of elliptic 1;
% b = short-axis of elliptic 1;
% k = long-axis of elliptic 2;
% l = short-axis of elliptic 2;
% example
% [x,y,z,t] = find_surface(30,-10,20,10,5,7,3)
% [x,y,z] is the value of point samples 
% t is computation consumption

function  [x,y,z,t]=find_surface_spl(alpha,beta,M,a,b,k,l)
tic;
% Projection on alpha=0/beta=0 surface
a_prime = a*cosd(alpha);
k_prime = k*cosd(beta);

% Adjust point density if you want higher surface quality
density = 500;
theta = linspace(0,180,181); % Think of top half of the geometry( symmetric with z-x plane)

% Space for saving point samples
x = zeros(length(theta),density);
% x_1 = zeros(length(theta),density/2);
% x_2 = zeros(length(theta),density/2);
y = zeros(length(theta),density);
% y_1 = zeros(length(theta),density/2);
% y_2 = zeros(length(theta),density/2);
z = zeros(length(theta),density);
z_1 = zeros(length(theta),density/2);
z_2 = zeros(length(theta),density/2);

% find value of point samples
for i=1:length(theta)
    if theta(i)<90
        X_1 = sqrt(1/(1/a_prime^2+(tand(theta(i))/b)^2));
        Y_1 = tand(theta(i))*X_1;
        X_2 = sqrt(1/(1/k_prime^2+(tand(theta(i))/l)^2));
        Y_2 = tand(theta(i))*X_2;
    elseif theta(i)==90
        X_1 = 0;
        Y_1 = b;
        X_2 = 0;
        Y_2 = l;
    elseif theta(i)>90
        X_1 = -sqrt(1/(1/a_prime^2+(tand(theta(i))/b)^2));
        Y_1 = tand(theta(i))*X_1;
        X_2 = -sqrt(1/(1/k_prime^2+(tand(theta(i))/l)^2));
        Y_2 = tand(theta(i))*X_2;
    end
        
    Z_1 = X_1*tand(alpha);
    Z_2 = M+X_2*tand(beta);
    Z_3 = Z_1+2*M;
    R_1 = sqrt(X_1^2+Y_1^2);
    R_2 = sqrt(X_2^2+Y_2^2);
    z(i,:)=linspace(Z_2-4*M,Z_2+6*M,density);
    z_points = [Z_2-4*M, Z_1-2*M, Z_2-2*M, Z_1, Z_2, Z_1+2*M, Z_2+2*M, Z_1+4*M, Z_2+4*M, Z_1+6*M, Z_2+6*M];
    r_points = [R_2, R_1, R_2, R_1, R_2, R_1, R_2, R_1, R_2, R_1, R_2];
    r = spline(z_points, r_points, z(i,:));
    x(i,:) = r*cosd(theta(i));
    y(i,:) = r*sind(theta(i));
end

% Check if the geometry exists
XNaN = isnan(x);
YNaN = isnan(y);
ZNaN = isnan(z);
bx = sum(XNaN,'all');
by = sum(YNaN,'all');
bz = sum(ZNaN,'all');
if bx~=0 ||by~=0 || bz~=0
    msg = "The geometry does not exist for these input parameters.";
    error(msg);
end

% Plots the surface

%plot_surface(x,y,z,M);

figure(1)
s_1 = surf(z,x,y);
s_1.EdgeColor = 'none';
hold on
s_2 = surf(z,x,-y);
s_2.EdgeColor = 'none';
axis equal
xlim([0,4*M])
xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')
hold off

t=toc;
end