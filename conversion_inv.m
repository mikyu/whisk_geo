function [M_prime,Gamma,At,Ac,eps,Phi,T] = conversion_inv(alpha,beta,M,a,b,k,l)
%conversion_inv.m Summary of this function goes here
%   Inverse function of conversion.m

T = b + l;
C = a.*cosd(alpha) + k.*cosd(beta);

M_prime = M./T;
Gamma = C./T;
At = (b - l)./T;
Ac = (a.*cosd(alpha) - k.*cosd(beta))./T;
eps = 2*a.*sind(alpha)./M;
Phi = (M - a.*sind(alpha) + k.*sind(beta))./M;

%disp([M_prim,Gamma,At,Ac,eps,Phi,T]);

end

