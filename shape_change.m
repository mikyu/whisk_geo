% Shape Change Function
% 'alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1' = original parameters
% 'alpha_2,beta_2,M_2,a_2,b_2,k_2,l_2' = new parameters
% 'step' = number of steps;
% 'time' = the time gap between each step
% example
% shape_change(0,0,10,6,4,3,1,20,-30,20,10,5,7,3,20,0)

function shape_change(alpha_1,beta_1,M_1,a_1,b_1,k_1,l_1,alpha_2,beta_2,M_2,a_2,b_2,k_2,l_2,step,time)

% Parameters changing steps

alpha = linspace(alpha_1,alpha_2,step);
beta = linspace(beta_1,beta_2,step);
M = linspace(M_1,M_2,step*2);
a = linspace(a_1,a_2,step);
b = linspace(b_1,b_2,step);
k = linspace(k_1,k_2,step);
l = linspace(l_1,l_2,step);

num=0;
Location=cell(5*step-3,3);
% First change geometry for elliptics
for i=1:step
    [x,y,z] = find_surface_sin(alpha_1,beta_1,M_1,a(i),b(i),k(i),l(i));
    num=num+1;
    Location{num,1}=x;
    Location{num,2}=y;
    Location{num,3}=z;
end

% Then change the distance of ellptics
for i=2:step*2 
    [x,y,z] = find_surface_sin(alpha(end),beta(end),M(i),a(end),b(end),k(end),l(end));
    num = num+1;
    Location{num,1} = x;
    Location{num,2} = y;
    Location{num,3} = z;
end

% Finally change the angle of elliptics respectively
for i=2:step 
    [x,y,z] = find_surface_sin(alpha_1,beta(i),M_1,a(end),b(end),k(end),l(end));
    num=num+1;
    Location{num,1} = x;
    Location{num,2} = y;
    Location{num,3} = z;
end

for i=2:step
    [x,y,z] = find_surface_sin(alpha(i),beta(end),M_1,a(end),b(end),k(end),l(end));
    num = num+1;
    Location{num,1} = x;
    Location{num,2} = y;
    Location{num,3} = z;
end

figure(1)

xlabel('z-direction')
ylabel('x-direction')
zlabel('y-direction')
for n=1:num
    if n<=step
        j=1;
    elseif n>3*step-1
        j=length(M);
    else
        j=n-step+1;
    end
    figure(1)    
    s_1 = surf(Location{n,3},Location{n,1},Location{n,2});
    s_1.EdgeColor = 'none';
    hold on
    s_2 = surf(Location{n,3},Location{n,1},-Location{n,2});
    s_2.EdgeColor = 'none';
    axis equal
    xlim([-M(j),3*M(j)])
    hold off
    pause(time)
end

end
