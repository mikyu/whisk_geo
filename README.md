# whisk_geo

whisk_geo is a set of codes which generate the coordinates and plots of a seal whisker.

- **shape_change.m**:   it transforms the shape of a whicker from one to another
- **find_surface.m**:   it generates the coordinates of a seal whisker
- **conversion.m**:     it converts the nondimensional paramters to "Hanke" paramters

Tested data are stored in the .cvs file.