function [X,Y,Z,t] = find_surface_sin_v(alpha,beta,M,a,b,k,l)
%find_surface_sin_v.m Summary of this function goes here
%   vector version of find_surface_sin.m
% alpha = rotated angle of elliptic 1 in deg.;
% beta = rotated angle of elliptic 2 in deg.;
% alpha,beta \in (-90,90)
% M = distance of center point of two elliptics;
% a = long-axis of elliptic 1;
% b = short-axis of elliptic 1;
% k = long-axis of elliptic 2;
% l = short-axis of elliptic 2;
% example
% [x,y,z,t] = find_surface_sin_v(30,-10,20,10,5,7,3)
% [x,y,z] is the value of point samples 
% t is computation consumption

tic;
% Projection on alpha=0/beta=0 surface
a_prime = a.*cosd(alpha);
k_prime = k.*cosd(beta);

% Adjust point density if you want higher surface quality
density = 500;
theta_1 = linspace(0,89,90); % Think of top half of the geometry( symmetric with z-x plane)
theta_2 = 90;
theta_3 = linspace(91,180,90);
theta = repmat([theta_1, theta_2, theta_3],density,1);

X_1_1 = sqrt(1./(1./a_prime.^2 + (tand(theta_1)./b).^2));
X_1_2 = sqrt(1./(1./k_prime.^2 + (tand(theta_1)./l).^2));
Y_1_1 = tand(theta_1).*X_1_1;
Y_1_2= tand(theta_1).*X_1_2;

X_2_1 = 0;
X_2_2 = 0;
Y_2_1 = b;
Y_2_2 = l;

X_3_1 = -sqrt(1./(1./a_prime^2 + (tand(theta_3)./b).^2));
X_3_2 = -sqrt(1./(1./k_prime^2 + (tand(theta_3)./l).^2));
Y_3_1 = tand(theta_3).*X_3_1;
Y_3_2 = tand(theta_3).*X_3_2;

X_1 = [X_1_1 X_2_1 X_3_1];
X_2 = [X_1_2 X_2_2 X_3_2];
Y_1 = [Y_1_1 Y_2_1 Y_3_1];
Y_2 = [Y_1_2 Y_2_2 Y_3_2];

Z_1 = X_1.*tand(alpha);
Z_2 = M+X_2.*tand(beta);
Z_3 = Z_1+2.*M;
R_1 = repmat(sqrt(X_1.^2+Y_1.^2),density/2,1);
R_2 = repmat(sqrt(X_2.^2+Y_2.^2),density/2,1);
M_1 = Z_2-Z_1;
M_2 = 2*M-M_1;
lam_1 = 2*M_1;
lam_2 = 2*M_2;
T_1 = repmat(2*pi./(lam_1),density/2,1);
T_2 = repmat(2*pi./(lam_2),density/2,1);

dZ_int_col = (0:density/2-1)';
dZ_int = repmat(dZ_int_col,1,length(X_1));
dz_12 = (Z_2-Z_1)/(density/2-1);
dZ_12 = dZ_int.*repmat(dz_12,density/2,1);
dz_23 = (Z_3-Z_2)/(density/2-1);
dZ_23 = dZ_int.*repmat(dz_23,density/2,1);
z = [dZ_12 + repmat(Z_1,density/2,1); dZ_23 + repmat(Z_2,density/2,1)];
r_1 = (R_1-R_2)./2.*cos(T_1.*dZ_12)+(R_1+R_2)/2;
r_2 = (R_1-R_2)./2.*-cos(T_2.*dZ_23)+(R_1+R_2)/2;
r = [r_1;r_2];

x = r.*cosd(theta);
y = r.*sind(theta);

x = x';
y = y';
z = z';

%[x_p,y_p,z_p,~] = find_surface_sin(30,-10,20,10,5,7,3);

X=repmat(x,1,4);
Y=repmat(y,1,4);
Z=[z-2*M,z,z+2*M,z+4*M];

% Plots the surface

% plot_surface(X,Y,Z,M);

t = toc;
end

