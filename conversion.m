function [alpha,beta,M,a,b,k,l] = conversion(M_prime,Gamma,Ac,At,eps,Phi,T)
%conversion.m 
%   This function converts the non-dimensional parameters to "Hanke"
%   parameters. The default value of T is 1

if nargin<7
    T = 1;
end

M = M_prime.*T;

l = T.*(1-At)./2;
b = T - l;

C = Gamma.*T;
c1 = (Ac./T + C)/2;
L1 = eps.*M/2;
a = sqrt(c1.^2 + L1.^2);
c2 = c1 - Ac.*T;
L2 = Phi.*M - M + L1;
k = sqrt(c2.^2 + L2.^2);

alpha = asind(L1./a);
beta = asind(L2./k);

%disp([alpha,beta,M,a,b,k,l]);

end